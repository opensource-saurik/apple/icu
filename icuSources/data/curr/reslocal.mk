#
# Local Apple addition for curr resources
# Copyright (c) 2012-2016 Apple Inc. All rights reserved.
#

CURR_SOURCE_LOCAL = ars.txt ckb.txt iu.txt ms_Arab.txt\
	ms_Arab_BN.txt tg.txt tk.txt\
	en_AD.txt en_AL.txt en_BA.txt en_BR.txt en_CZ.txt\
	en_EE.txt en_ES.txt en_FR.txt en_GR.txt en_HR.txt\
	en_HU.txt en_IS.txt en_IT.txt en_JP.txt en_LT.txt\
	en_LU.txt en_LV.txt en_ME.txt en_MV.txt en_NO.txt\
	en_PL.txt en_PT.txt en_RO.txt en_RU.txt en_SK.txt\
	en_TR.txt es_BZ.txt

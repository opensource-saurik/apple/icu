﻿// ***************************************************************************
// *
// * Copyright (C) 2015 International Business Machines
// * Corporation and others. All Rights Reserved.
// * Tool: org.unicode.cldr.icu.NewLdml2IcuConverter
// * Source File: <path>/common/main/iu_CA.xml
// *
// ***************************************************************************
iu_CA{
    Version{"2.1.19.14"}
}
